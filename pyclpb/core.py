# encoding: utf-8
def compute_vars(f):
    def func(self, *args):
        if not self._vars:
            self._vars = self.collect_vars()
        return f(self, *args)
    func.__name__ = f.__name__
    return func


class BooleanExpression(object):
    _vars = None

    def __call__(self, binding):
        raise NotImplementedError

    @compute_vars
    def var(self, n):
        return self._vars[n]

    @compute_vars
    def nvars(self):
        return len(self._vars)
    __len__ = nvars

    def collect_vars(self):
        raise NotImplementedError


class BinaryExpression(object):
    def __init__(self, lhs, rhs):
        self.lhs = lhs
        self.rhs = rhs

    def __str__(self):
        return '(%s %s %s)' % (self.lhs, self.symbol, self.rhs)
    __repr__ = __str__

    def collect_vars(self):
        lhs = self.lhs.collect_vars()
        rhs = self.rhs.collect_vars()
        for e in rhs:
            if e in lhs:
                continue
            lhs.append(e)
        return lhs


class Negation(BooleanExpression):
    def __init__(self, child):
        self.child = child

    def __call__(self, binding):
        return not self.child(binding)

    def __str__(self):
        return '¬%s' % (self.child)
    __repr__ = __str__

    def collect_vars(self):
        return self.child.collect_vars()
Not = Negation


class Conjunction(BinaryExpression, BooleanExpression):
    symbol = '∧'
    def __call__(self, binding):
        lhs = self.lhs(binding)
        if not lhs:
            return False
        rhs = self.rhs(binding)
        return lhs and rhs
And = Conjunction


class Disjunction(BinaryExpression, BooleanExpression):
    symbol = '∨'
    def __call__(self, binding):
        lhs = self.lhs(binding)
        if lhs:
            return True
        rhs = self.rhs(binding)
        return rhs
Or = Disjunction


class Implication(BinaryExpression, BooleanExpression):
    symbol = '⇒'
    def __call__(self, binding):
        lhs = self.lhs(binding)
        if not lhs:
            return True
        rhs = self.rhs(binding)
        return rhs


class Equivalence(BinaryExpression, BooleanExpression):
    symbol = '⇔'
    def __call__(self, binding):
        lhs = self.lhs(binding)
        rhs = self.rhs(binding)
        return lhs == rhs


class Variable(BooleanExpression):
    i = 0
    labels = set()

    def __init__(self, label=None):
        if label is None:
            label = self.compute_label()
        self.label = label
        assert label not in self.labels
        self.labels.add(label)

    def __call__(self, binding):
        return binding[self]

    def __str__(self):
        return '(%s)' % self.label
    __repr__ = __str__

    def compute_label(self):
        label = 'v%s' % self.i
        while label in self.labels:
            self.i += 1
            label = 'v%s' % self.i
        return label

    def collect_vars(self):
        return [self]

    @classmethod
    def reset(cls):
        cls.i = 0
        cls.labels = set()
