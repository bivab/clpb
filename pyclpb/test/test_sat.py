import py
from pyclpb.core import Variable, Not, And, Equivalence, Or
from pyclpb.bdd import bdd, Leaf, InnerNode
class TestBDDSAT(object):

    def test_one_solution(self):
        v = Variable()
        b = bdd(Or(v, Not(v)))
        assert b.satcount() == 1

    def test_variable(self):
        b = bdd(Variable())
        assert b.satcount() == 1

    def test_negated_variable(self):
        v = Variable()
        b = bdd(And(v, Not(v)))
        assert b.satcount() == 0

    def test_3_solutions(self):
        be = Or(Variable(), Not(Variable()))
        b = bdd(be)
        assert b.satcount() == 3

