# encoding: utf-8
from pyclpb.core import Variable, Not, And, Equivalence, Or
from pyclpb.bdd import bdd


class TestBDD(object):

    def test_simple_bdd(self):
        v = Variable()
        b = bdd(v)
        assert b.high.label == 1
        assert b.low.label == 0

    def test_simple_negation(self):
        n = Not(Variable())
        b = bdd(n)
        assert b.high.label == 0
        assert b.low.label == 1

    def test_simple_conjunction(self):
        a = And(Variable(), Variable())
        b = bdd(a)
        assert b.low.label == 0
        assert b.high.low.label == 0
        assert b.high.low is b.low
        assert b.high.high.label == 1

    def test_complex(self):
        # (v0 ⇔ v1) ∧ (v2 ⇔ v3)
        v = [Variable() for x in range(4)]
        e = [Equivalence(v[i], v[i+1]) for i in range(0, 4, 2)]
        a = And(e[0], e[1])
        b = bdd(a)
        assert b.low.low.low.low is b.high.high.high.high
        assert b.low.high.label == 0

    def test_or_chain(self):
        # v0 ∨ ¬v1 ∨ v2
        v = [Variable() for x in range(3)]
        f = Or(v[0], Or(Not(v[1]), v[2]))
        b = bdd(f)
        assert b.high.label == 1
        assert b.low.low.label == 1
        assert b.low.high.high.label == 1
