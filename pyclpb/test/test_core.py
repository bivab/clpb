import py
from pyclpb.core import Variable, Negation, And, Or, Implication, Equivalence

class FakeVariable(object):
    def __call__(self, *args):
        raise Exception

class TestEvaluation(object):
    def test_variable(self):
        v = Variable()
        assert v({v: False}) == False
        assert v({v: True}) == True

    def test_negatation(self):
        v = Variable()
        assert Negation(v)({v: False}) == True
        assert Negation(v)({v: True}) == False

    def test_conjunction(self):
        v1 = Variable()
        v2 = Variable()
        assert And(v1, v2)({v1: True,  v2: True})  == True
        assert And(v1, v2)({v1: False, v2: True})  == False
        assert And(v1, v2)({v1: True,  v2: False}) == False
        assert And(v1, v2)({v1: False, v2: False}) == False

    def test_conjunction_short_circuiting(self):
        v1 = Variable()
        v2 = FakeVariable()
        assert And(v1, v2)({v1: False,  v2: True})  == False

    def test_disjunction_short_circuiting(self):
        v1 = Variable()
        v2 = FakeVariable()
        assert Or(v1, v2)({v1: True,  v2: False})  == True

    def test_disjunction(self):
        v1 = Variable()
        v2 = Variable()
        assert Or(v1, v2)({v1: True,  v2: True})  == True
        assert Or(v1, v2)({v1: False, v2: True})  == True
        assert Or(v1, v2)({v1: True,  v2: False}) == True
        assert Or(v1, v2)({v1: False, v2: False}) == False

    def test_implication(self):
        v1 = Variable()
        v2 = Variable()
        assert Implication(v1, v2)({v1: True,  v2: True})  == True
        assert Implication(v1, v2)({v1: False, v2: True})  == True
        assert Implication(v1, v2)({v1: True,  v2: False}) == False
        assert Implication(v1, v2)({v1: False, v2: False}) == True

    def test_equivalence(self):
        v1 = Variable()
        v2 = Variable()
        assert Equivalence(v1, v2)({v1: True,  v2: True})  == True
        assert Equivalence(v1, v2)({v1: False, v2: True})  == False
        assert Equivalence(v1, v2)({v1: True,  v2: False}) == False
        assert Equivalence(v1, v2)({v1: False, v2: False}) == True


class TestExpression(object):
    def test_get_vars_of_var(self):
        v = Variable()
        assert v.var(0) is v
        assert v.nvars() == 1

    def test_vars_of_binary_expression(self):
        v1 = Variable()
        v2 = Variable()
        e = Equivalence(v1, v2)
        assert e.var(0) is v1
        assert e.var(1) is v2
        assert e.nvars() == 2

    def test_vars_repeated(self):
        v = Variable()
        e = Equivalence(v, v)
        assert e.var(0) is v
        assert py.test.raises(Exception, 'e.var(1)')
        assert e.nvars() == 1

    def test_vars_repeated2(self):
        v = Variable()
        v2 = Variable()
        e = Or(Negation(v), Equivalence(v2, v))
        assert e.var(0) is v
        assert e.var(1) is v2
        assert e.nvars() == 2


def test_variable_labels():
    Variable.reset()
    v = Variable()
    v2 = Variable()
    assert v.label == 'v0'
    assert v2.label == 'v1'
    assert py.test.raises(Exception, 'Variable("v1")')
