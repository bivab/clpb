def bdd(expression):
    builder = BDD(expression)
    b = builder.build(0, {})
    return b

class Node(object):
    def __init__(self, mappings):
        self.mappings = mappings

    def satcount(self):
        raise NotImplementedError

class InnerNode(Node):
    def __init__(self, i, l, h, mappings):
        Node.__init__(self, mappings)
        self.i = i
        self.low = l
        self.high = h

    # return variable index of a node
    def var(self):
        return self.mappings['t'][self][0]

    def __str__(self):
        return '(node: %s)' % (self.i)
    __repr__ = __str__

    def satcount(self):
        res = (2 ** (self.low.var() - self.var() - 1) *
                self.low.satcount())
        res += (2 ** (self.high.var() - self.var() - 1) *
                self.high.satcount())
        return res


class Leaf(Node):
    def __init__(self, label, mappings, expression):
        Node.__init__(self, mappings)
        self.label = label
        self.expression = expression

    def __str__(self):
        return "[%s]" % self.label

    def satcount(self):
        if self.label == 0:
            return 0
        elif self.label == 1:
            return 1

    # return variable index of a node
    # for leaves it is n + 1, where n is the number of variables in the
    # original formula
    def var(self):
        return len(self.expression)


    __repr__ = __str__
class BDD(object):


    def __init__(self, expression):
        self.expression = expression
        self.nodes = []
        self.mappings = {'h': {}, 't': {}}
        self.leaf0 = Leaf(0, {'h': self.h, 't': self.t}, self.expression)
        self.leaf1 = Leaf(1, {'h': self.h, 't': self.t}, self.expression)

    @property
    def h(self):
        return self.mappings['h']

    @property
    def t(self):
        return self.mappings['t']

    def build(self, i, binding):
        try:
            if self.expression(binding):
                return self.leaf1
            else:
                return self.leaf0
        except Exception, e:
            pass
            #XXX use propper exception
        var = self.expression.var(i)
        binding[var] = False
        v0 = self.build(i + 1, binding)
        binding[var] = True
        v1 = self.build(i + 1, binding)
        del binding[var]
        return self.mk(i, v0, v1)

    def mk(self, i, l, h):
        if l is h:
            return l
        elif (i, l, h) in self.h:
            return self.h[(i, l, h)]
        else:
            key = (i, l, h)
            n = InnerNode(i, l, h, {'h': self.h, 't': self.t})
            self.t[n] = key
            self.h[key] = n
            return n
